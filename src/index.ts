let cv = document.getElementById("cv") as HTMLCanvasElement
let ctx = cv.getContext("2d") as CanvasRenderingContext2D


let eau_button = document.getElementById("eau") as HTMLButtonElement
let vide_button = document.getElementById("vide") as HTMLButtonElement
let obstacle_button = document.getElementById("obstacle") as HTMLButtonElement
let output_button = document.getElementById("output") as HTMLButtonElement
let area_output = document.getElementById("area") as HTMLTextAreaElement

let witdh_input = document.getElementById("width") as HTMLInputElement
let height_input = document.getElementById("height") as HTMLInputElement
let size_input = document.getElementById("size") as HTMLInputElement
size_input.value = "10"
witdh_input.value = "100"
height_input.value = "100"


let size = parseInt(size_input.value)
let mode = 1
let tracing = false

size_input.addEventListener("change", update)
witdh_input.addEventListener("change", update)
height_input.addEventListener("change", update)
eau_button.addEventListener("click", () => {
    mode = 1
})
vide_button.addEventListener("click", () => {
    mode = 0
})
obstacle_button.addEventListener("click", () => {
    mode = 2
})


function fill(grd: Array<Array<number>> = [[]], width: number, height: number) {
    let out = [] as Array<Array<number>>
    for (let y = 0; y < height; ++y) {
        let line = [] as Array<number>
        for (let x = 0; x < width; ++x) {
            if (x==0 || x == width-1 || y == height-1){
                line.push(2) // obstacle

            }
            else if (y < grd.length && x < grd[y].length) {
                line.push(grd[y][x])
            }
            else {
                line.push(0)
            }
        }
        out.push(line)
    }
    return out

}
let grid = fill(undefined, parseInt(witdh_input.value), parseInt(height_input.value))


function update() {
    size = parseInt(size_input.value)
    cv.width = parseInt(witdh_input.value) * size
    cv.height = parseInt(height_input.value) * size
    if (cv.height / size != grid.length || cv.width / size != grid.length) {
        grid = fill(grid, cv.width / size, cv.height / size)
    }
    show()
}


function show() {
    for (let y = 0; y < grid.length; ++y) {
        for (let x = 0; x < grid[y].length; ++x) {
            let color = "#ffffff"
            switch (grid[y][x]) {
                case 0:
                    color = "#a6cee3"
                    break
                case 1:
                    color = "#2077b4"
                    break
                case 2:
                    color = "#e21a1c"
                    break
            }
            ctx.fillStyle = color
            ctx.fillRect(x * size, y * size, size, size)
        }
    }

}


cv.addEventListener("mousedown", (ev) => {
    let x = Math.floor(ev.offsetX / size)
    let y = Math.floor(ev.offsetY / size)
    tracing = true
    grid[y][x] = mode
    show()

})

cv.addEventListener("mousemove", (ev) => {
    if (tracing) {
        let y = Math.floor(ev.offsetY / size)
        let x = Math.floor(ev.offsetX / size)
        grid[y][x] = mode
        show()
    }

})

cv.addEventListener("mouseleave", () => {
    tracing = false
})

cv.addEventListener("mouseup", () => {
    tracing = false
})

output_button.addEventListener("click", () => {
    area_output.value = JSON.stringify(grid)
})






update()
show()